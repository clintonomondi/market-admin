import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';



Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './view/App'
import Login from './pages/login'
import Home from './pages/home'
import Categories from './category/categories'
import SubCategories from './category/subcategory'
import Users from './users/users'
import Clients from './users/clients'
import Ads from './ads/ads'
import Password from './pages/password'
import Forget from './pages/forget'
import Hotels from './hotel/hotels'
import Invoice from './invoice/invoices'
import Select from './invoice/select'
import Print from './invoice/print'



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },

        {
            path: '/categories',
            name: 'categories',
            component: Categories,
        },
        {
            path: '/category/:id',
            name: 'subcategories',
            component: SubCategories,
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/clients',
            name: 'clients',
            component: Clients,
        },
        {
            path: '/ads',
            name: 'ads',
            component: Ads,
        },
        {
            path: '/password',
            name: 'password',
            component: Password,
        },
        {
            path: '/password/forget',
            name: 'forget',
            component: Forget,
            meta: { hideNavigation: true }
        },
        {
            path: '/hotels',
            name: 'hotels',
            component: Hotels,
        },
        {
            path: '/invoices',
            name: 'invoices',
            component: Invoice,
        },
        {
            path: '/invoices/select',
            name: 'select',
            component: Select,
        },
        {
            path: '/invoices/print/:id',
            name: 'print',
            component: Print,
        },


    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

